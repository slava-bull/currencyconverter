package com.slava_bull.currencyconverter.currency;

import java.util.ArrayList;

public class CurrencyRate extends Currency {
    protected int nominal;
    protected double value;

    public CurrencyRate(String name, String charCode, int nominal, double value) {
        super(name, charCode);
        this.nominal = nominal;
        this.value = value;
    }

    public double getValue() {
        return value / nominal;
    }
}
