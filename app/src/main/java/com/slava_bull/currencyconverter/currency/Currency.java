package com.slava_bull.currencyconverter.currency;

public class Currency {
    protected String name;
    protected String charCode;

    public Currency(String name, String charCode) {
        this.name = name;
        this.charCode = charCode;
    }

    @Override
    public String toString() {
        return name;
    }

    public String toChar(){
        return charCode;
    }
}
