package com.slava_bull.currencyconverter.currency;

import java.util.ArrayList;

public class CBRCurrency extends CurrencyRate {
    protected String ID;
    protected int numCode;

    public CBRCurrency(String name, String charCode, int nominal, double value, String ID, int numCode) {
        super(name, charCode, nominal, value);
        this.ID = ID;
        this.numCode = numCode;
    }

    public String getID() {
        return ID;
    }

    public int getNumCode() {
        return numCode;
    }
}
