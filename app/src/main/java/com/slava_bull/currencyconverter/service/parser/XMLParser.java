package com.slava_bull.currencyconverter.service.parser;

import com.slava_bull.currencyconverter.currency.CBRCurrency;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;

public class XMLParser {
    private ArrayList<CBRCurrency> currencies;

    public XMLParser() {
        currencies = new ArrayList<>();
    }

    public ArrayList<CBRCurrency> getCurrencyList() {
        return currencies;
    }

    public void parse(Document doc) {
        NodeList nList = doc.getElementsByTagName("Valute");

        for (int i = 0; i < nList.getLength(); i++) {
            Node node = nList.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                CBRCurrency cbrCurrency = getCbrCurrency(element);
                currencies.add(cbrCurrency);
            }
        }
    }

    private CBRCurrency getCbrCurrency(Element element) {
        return new CBRCurrency(
                element.getElementsByTagName("Name").item(0).getTextContent(),
                element.getElementsByTagName("CharCode").item(0).getTextContent(),
                Integer.valueOf(element.getElementsByTagName("Nominal").item(0).getTextContent()),
                Double.valueOf(element.getElementsByTagName("Value").item(0).getTextContent().replace(",", ".")),
                element.getAttribute("ID"),
                Integer.valueOf(element.getElementsByTagName("NumCode").item(0).getTextContent())
        );
    }
}
