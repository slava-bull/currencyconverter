package com.slava_bull.currencyconverter.service;

import android.os.AsyncTask;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class XMLLoader extends AsyncTask<URL,Void, Document> {
    @Override
    protected Document doInBackground(URL... urls) {
        try {
            return load(urls[0]);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Document load(URL url) throws IOException, SAXException, ParserConfigurationException {
        URLConnection uc = url.openConnection();
        InputStream is = uc.getInputStream();

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(is);

        doc.getDocumentElement().normalize();
        return doc;
    }
}
