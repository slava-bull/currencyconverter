package com.slava_bull.currencyconverter.service;

import com.slava_bull.currencyconverter.currency.CBRCurrency;
import com.slava_bull.currencyconverter.currency.Currency;
import com.slava_bull.currencyconverter.currency.CurrencyRate;
import com.slava_bull.currencyconverter.service.parser.XMLParser;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class CurrencyService {
    private final String strUrl = "http://www.cbr.ru/scripts/XML_daily.asp";
    private ArrayList<CBRCurrency> currencies;

    public CurrencyService() throws IOException, ExecutionException, InterruptedException {
        URL url = new URL(strUrl);

        Document doc = getDoc(url);
        if(doc==null) throw new ConnectException("No connection!");
        parse(doc);
    }

    private Document getDoc(URL url) throws ExecutionException, InterruptedException {
        XMLLoader loader = new XMLLoader();
        loader.execute(url);
        return loader.get();
    }

    private void parse(Document doc) {
        XMLParser parser = new XMLParser();
        parser.parse(doc);
        currencies = parser.getCurrencyList();
    }

    public ArrayList<CurrencyRate> getCurrencies() {
        return new ArrayList<CurrencyRate>(currencies);
    }
}
