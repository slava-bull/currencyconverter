package com.slava_bull.currencyconverter;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;

import com.slava_bull.currencyconverter.adapter.SpinnerAdapter;
import com.slava_bull.currencyconverter.convertor.Convertor;
import com.slava_bull.currencyconverter.currency.Currency;
import com.slava_bull.currencyconverter.currency.CurrencyRate;
import com.slava_bull.currencyconverter.service.CurrencyService;

import org.xml.sax.SAXException;

import java.io.IOException;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import javax.xml.parsers.ParserConfigurationException;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Spinner spinner1, spinner2;
    private EditText et1, et2;
    private ArrayList<CurrencyRate> currencyRates = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadCurrency();
        initViews();
    }

    private void initViews() {
        btnInit();
        spinnerInit();
        editTextInit();
    }

    private void editTextInit() {
        et1 = findViewById(R.id.firstET);
        et2 = findViewById(R.id.secondET);
    }

    private void spinnerInit() {
        spinner1 = findViewById(R.id.firstSpinner);
        spinner2 = findViewById(R.id.secondSpinner);
        spinner1.setAdapter(new SpinnerAdapter(this, R.layout.spinner_currency, new ArrayList<Currency>(currencyRates)));
        spinner2.setAdapter(new SpinnerAdapter(this, R.layout.spinner_currency, new ArrayList<Currency>(currencyRates)));
    }

    private void btnInit() {
        (findViewById(R.id.btnCalc)).setOnClickListener(this);
        (findViewById(R.id.btnSwap)).setOnClickListener(this);
    }

    private void loadCurrency() {
        try {
            CurrencyService service = new CurrencyService();
            currencyRates = service.getCurrencies();
        } catch (ConnectException e) {
            showNoConnectionDialog();
        }catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    private void showNoConnectionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Error!")
                .setMessage("No Connection!")
                .setIcon(android.R.drawable.stat_notify_error)
                .setCancelable(false)
                .setNegativeButton("ОК",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnCalc:
                calculate();
                break;
            case R.id.btnSwap:
                swap();
                break;
        }
    }

    private void calculate() {
        Convertor convertor = getConvertor();
        double value = convertor.convert(Double.valueOf(et1.getText().toString()));
        et2.setText(String.format(Locale.ROOT, "%.2f", value));
    }

    private Convertor getConvertor() {
        CurrencyRate from = (CurrencyRate) spinner1.getSelectedItem();
        CurrencyRate to = (CurrencyRate) spinner2.getSelectedItem();
        return Convertor.getConvertor(from, to);
    }

    private void swap() {
        int pos1 = spinner1.getSelectedItemPosition();
        int pos2 = spinner2.getSelectedItemPosition();
        spinner1.setSelection(pos2);
        spinner2.setSelection(pos1);

        String tmp = et1.getText().toString();
        et1.setText(et2.getText());
        et2.setText(tmp);
    }
}
