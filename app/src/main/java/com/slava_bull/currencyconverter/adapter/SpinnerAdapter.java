package com.slava_bull.currencyconverter.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.slava_bull.currencyconverter.R;
import com.slava_bull.currencyconverter.currency.Currency;

import java.util.List;

public class SpinnerAdapter extends ArrayAdapter<Currency> {
    private Context context;

    public SpinnerAdapter(@NonNull Context context, int resource, List<Currency> objects) {
        super(context, resource, objects);
        this.context = context;
    }

    private static class ViewHolder {
        TextView tvChar;
        TextView tvStr;
    }

    @SuppressLint("ViewHolder")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater == null) return super.getView(position, convertView, parent);
        convertView = inflater.inflate(android.R.layout.simple_spinner_item, parent, false);


        Currency currency = getItem(position);
        ((TextView) convertView.findViewById(android.R.id.text1)).setText(currency != null ? currency.toChar() : "");
        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final SpinnerAdapter.ViewHolder viewHolder;
        final Currency currency = getItem(position);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (inflater == null) return null;

            convertView = inflater.inflate(R.layout.spinner_currency, parent, false);
            viewHolder = new SpinnerAdapter.ViewHolder();

            viewHolder.tvChar = convertView.findViewById(R.id.tvChar);
            viewHolder.tvStr = convertView.findViewById(R.id.tvStr);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (SpinnerAdapter.ViewHolder) convertView.getTag();
        }

        if (currency != null) {
            viewHolder.tvChar.setText(currency.toChar());
            viewHolder.tvStr.setText(currency.toString());
        }

        return convertView;
    }
}
