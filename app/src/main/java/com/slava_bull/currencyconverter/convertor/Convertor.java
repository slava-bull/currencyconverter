package com.slava_bull.currencyconverter.convertor;

import com.slava_bull.currencyconverter.currency.CurrencyRate;

public abstract class Convertor {

    public static Convertor getConvertor(CurrencyRate from, CurrencyRate to){
        return new CurrencyConvertor(from,to);
    }

    public abstract double convert();

    public abstract double convert(double value);
}
