package com.slava_bull.currencyconverter.convertor;

import com.slava_bull.currencyconverter.currency.CurrencyRate;

public class CurrencyConvertor extends Convertor {

    protected double unitOfConversion;

    public CurrencyConvertor(CurrencyRate from, CurrencyRate to) {
        this.unitOfConversion = from.getValue() / to.getValue();
    }

    @Override
    public double convert() {
        return convert(1);
    }

    @Override
    public double convert(double value) {
        return value * unitOfConversion;
    }
}
